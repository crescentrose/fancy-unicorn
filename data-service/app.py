#!/usr/bin/env python3

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
from models import Accommodation, AccommodationReview, db
from responders import *
import os

load_dotenv()

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get('DATABASE_URL')
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False # this turns off a deprecation warning so I guess I need it?
db.init_app(app)

@app.route("/accommodations")
def accommodations():
    accommodations = Accommodation.query.all()

    return respond_with_result(accommodations)

@app.route("/accommodations/<id>")
def accommodation(id):
    accommodation = Accommodation.query.get(id)

    if accommodation is None:
        return respond_with_not_found("Accommodation", id)

    return respond_with_result(accommodation)

@app.route("/accommodations/<accommodation_id>/reviews")
def accommodation_reviews(accommodation_id):
    accommodation = Accommodation.query.get(accommodation_id)

    # Surely there's a better way for guard clauses like this in Python...
    if accommodation is None:
        return respond_with_not_found("Accommodation", accommodation_id)

    return respond_with_result(accommodation.reviews)

@app.route("/reviews/<id>")
def accommodation_review(id):
    review = AccommodationReview.query.get(id)

    if review is None:
        return respond_with_not_found("Review", id)

    return respond_with_result(review)

if __name__ == "__main__" and os.getenv('APP_ENV', 'development') == 'production':
    from waitress import serve
    import logging
    logger = logging.getLogger('waitress')
    logger.setLevel(logging.INFO)
    serve(app, host="0.0.0.0", port=os.getenv('PORT', 80))
