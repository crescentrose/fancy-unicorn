from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import JSONB, UUID
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Accommodation(db.Model):
    __tablename__ = 'accommodations'

    id = db.Column(UUID, primary_key = True)
    name = db.Column(db.String())

    def serialize(self):
        return {
            "id": self.id,
            "name": self.name
        }

class AccommodationReview(db.Model):
    __tablename__ = 'accommodation_reviews'

    id = db.Column(UUID, primary_key = True)
    user_id = db.Column(UUID)
    travel_date = db.Column(db.DateTime, nullable=False)
    general_rating = db.Column(db.Integer)
    aspect_ratings = db.Column(JSONB)

    accommodation_id = db.Column(UUID, db.ForeignKey('accommodations.id'), nullable = False)
    accommodation = db.relationship(
        'Accommodation',
        backref='reviews',
        primaryjoin='Accommodation.id==AccommodationReview.accommodation_id'
    )

    def serialize(self):
        return {
            "id": self.id,
            "accommodation_id": self.accommodation_id,
            "user_id": self.user_id,
            "travel_date": self.travel_date,
            "general_rating": self.general_rating,
            "aspect_ratings": self.aspect_ratings
        }
