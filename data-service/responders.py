from flask import jsonify, make_response

# I'm not really sure if this is idiomatic Python, but it saves me from
# repeating this code in every controller action so it can't be _that_ bad...

# The idea is that I'd pass either one or multiple objects here that respond to
# `serialize` (maybe that's the duck-typing Ruby dev in me) and then convert it
# either to a JSON array or to a JSON object, loosely based on the JSON:API spec
# https://jsonapi.org/format/#fetching
# TODO: This should have pagination (for performance reasons), or at least a
# limit.
def respond_with_result(object_or_list):
    if isinstance(object_or_list, list):
        data = list(map(lambda obj: obj.serialize(), object_or_list))
    else:
        data = object_or_list.serialize()

    response = {
        "data": data
    }

    return make_response(jsonify(response), 200)

# Likewise, loosely based on the JSON:API spec
# https://jsonapi.org/format/#errors
def respond_with_not_found(object_name, id):
    # The object name could probably be returned via a reflection or something
    response = {
        "errors": [
            {
                "status": 404,
                "title": f"{object_name} with ID {id} does not exist."
            }
        ]
    }

    return make_response(jsonify(response), 404)
