# Accommodation Application

🏄 Find the best vacation spot...

## Quick start

Start the provided `docker-compose.yml` file with `docker-compose up --build`.

Note that the `import-service` might restart once or twice on first startup -
this is because it's _so fast_! and Postgres doesn't have enough time to boot up
and generate the database. This could probably have been alleviated via the use
of Docker's healthcheck endpoints. Either way, the import service will
automatically keep retrying until it finds Postgres and imports the sample data.

### How to use

The scoring service is exposed on the port 5001 by default. So you can do
something like this:

```bash
curl 'localhost:5001/aspect-reviews/0a1c8c03-cf4e-4136-abc9-d8b00b84b1fe'
```

Or this:

```bash
curl 'localhost:5001/general-reviews/0a1c8c03-cf4e-4136-abc9-d8b00b84b1fe'
```

In case you wish to time travel, you can set the `FAKE_TIME` environment
variable on the scoring service container.

The data service is exposed on the port 5000 for your convenience.

The provided `import-service` will reset the database on every startup. This is
probably fine.

## Description

This project contains the following components:

* 🐍 a Python backend that reads accommodation and review data from the
    database, written with Flask and SQLAlchemy,
* ☕ a Node.js backend that consumes the Python API and crunches the data,
    producing weighted averages based on scores that decay over time, for given
    categories or for the overall score
* 💎 a Ruby (yes, I know...) script that will generate the database schema,
  read the JSON data dump and pipe it into the database.

The Python and Ruby parts of the project have comments around explaining why I
did things the way I did them. The Node.js part doesn't because one never
reveals all of their secrets.
