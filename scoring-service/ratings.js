const ratingWeight = (reviewDate, currentDate) => {
  // javascript's 0 based month indexing is making me want to pluck my eyes out
  let reviewFullMonths = (reviewDate.getFullYear() * 12 + (reviewDate.getMonth() + 1))
  let currentFullMonths = (currentDate.getFullYear() * 12 + (currentDate.getMonth() + 1))
  let monthDifference = currentFullMonths - reviewFullMonths;

  if (monthDifference > 24) {
    return Math.log(1.77)
  }

  return Math.log(25 - Math.max(monthDifference, 0))
}

// takes an array of arrays containing the data point and the weight attributed
// to that data point (e.g. if we have a rating of 8 and a weight of 1.5, then
// the input should be [[8, 1.5]]
const weightedAverage = (weightsAndData) => {
  dataWeightedSum = weightsAndData
    .filter(dataPoint => dataPoint[0] != null)
    .map(dataPoint => dataPoint[0] * dataPoint[1])
    .reduce((acc, val) => acc + val, 0);

  weightsSum = weightsAndData.reduce((acc, val) => acc + val[1], 0);


  if (dataWeightedSum == 0 || weightsSum == 0) {
    return null;
  }

  return (dataWeightedSum / weightsSum).toFixed(2);
}

module.exports = { weightedAverage, ratingWeight };
