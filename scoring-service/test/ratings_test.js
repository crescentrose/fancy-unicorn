const assert = require('assert');
const ratings = require('../ratings');

describe('Ratings', function() {
  describe('weightedAverage', function() {
    it('returns a weighted average when provided with data and weights', function() {
      let weightsAndData = [
        [8, 3.22],
        [8, 3.22],
        [5, 3.136],
        [10, 3.136],
        [4, 0.56]
      ];
      return assert.equal(ratings.weightedAverage(weightsAndData), 7.59)
    });
  });
  
  describe('ratingWeight', function() {
    it('returns the weight for the difference between two dates', function() {
      let reviewDate = new Date(2021, 0, 11, 15, 0, 0);
      let currentDate = new Date(2021, 7, 28, 11, 0, 0);
      return assert.equal(ratings.ratingWeight(reviewDate, currentDate), Math.log(18));
    });

    it('returns the  maximum weight for the difference of 0', function() {
      let reviewDate = new Date(2021, 7, 11, 15, 0, 0);
      let currentDate = new Date(2021, 7, 28, 11, 0, 0);
      return assert.equal(ratings.ratingWeight(reviewDate, currentDate), Math.log(25));
    });

    it('returns the lowest weight for all differences greater than 2 years', function() {
      let reviewDate = new Date(2018, 6, 11, 15, 0, 0);
      let currentDate = new Date(2021, 7, 28, 11, 0, 0);
      return assert.equal(ratings.ratingWeight(reviewDate, currentDate), Math.log(1.77));
    });
  });
});
