const express = require('express');
const port = process.env.PORT || 3000
const app = express();

const api = require('./api');
const ratings = require('./ratings');

// This logic is a bit messy - usually I'd put it in a service class or
// something similar to that. However, time!
app.get('/general-reviews/:uuid', (req, res) => {
  let currentDate = process.env.FAKE_DATE ? new Date(process.env.FAKE_DATE) : new Date();

  return api
    .getAccommodationReviews(req.params.uuid)
    .then(apiRes => apiRes.json())
    .then(json => {
      const weightsAndRatings = json.data.map(review => {
        return [
          review.general_rating,
          ratings.ratingWeight(new Date(review.travel_date), currentDate)
        ];
      });

      return res.json({
        weightedAverage: ratings.weightedAverage(weightsAndRatings)
      });
    })
    .catch(err => {
      console.log(err);
      return res.json({
        error: 'Something went wrong :('
      });
    });
});

app.get('/aspect-reviews/:uuid', (req, res) => {
  let currentDate = process.env.FAKE_DATE ? new Date(process.env.FAKE_DATE) : new Date();

  return api
    .getAccommodationReviews(req.params.uuid)
    .then(apiRes => apiRes.json())
    .then(json => {
      let aspects = {};
      for (const review in json.data) {
        let aspectRatings = json.data[review].aspect_ratings;
        for (const aspect in aspectRatings) {
          if (aspectRatings[aspect]) {
            if (!aspects.hasOwnProperty(aspect)) {
              aspects[aspect] = [];
            }
            aspects[aspect].push([
              aspectRatings[aspect],
              ratings.ratingWeight(new Date(json.data[review].travel_date), currentDate)
            ]);
          }
        }
      }

      let weightedAspects = Object.fromEntries(
        Object.entries(aspects)
              .map(([aspect, weightsAndRatings]) => {
                return [aspect, ratings.weightedAverage(weightsAndRatings)]
              })
      );

      return res.json(weightedAspects);
    })
    .catch(err => {
      console.log(err);
      return res.json({
        error: 'Something went wrong :('
      });
    });
});

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
