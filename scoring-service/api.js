const fetch = require('node-fetch');
const API_URL = process.env.API_URL || 'http://localhost:5000';

const getAccommodationReviews = (accommodationId) => {
  return fetch(API_URL + `/accommodations/${accommodationId}/reviews`);
};


module.exports = { getAccommodationReviews };
