#!/usr/bin/env ruby

require 'logger'
require 'sequel'
require 'oj'

logger = Logger.new(STDOUT)
logger.info('importer started')
DB = Sequel.connect(
  ENV.fetch('DATABASE_URL', 'postgres://localhost/import_service'), # provide a sensible default for the database URL
)

# Set up the tables. The `?` at the end means that they won't be created if they
# already exist.
# In a larger project I'd use migrations (and this is what I originally did
# anyway), but then I figured that would just be overengineering it for the sake
# of being clever.
def create_tables
  DB.create_table?(:accommodations) do
    uuid :id, primary_key: true
    String :name
  end

  DB.create_table?(:accommodation_reviews) do
    uuid :id, primary_key: true
    uuid :accommodation_id, foreign_key: true
    uuid :user_id
    timestamp :travel_date
    Integer :general_rating
    jsonb :aspect_ratings
  end
end

# Helper method for clarity
def json_from_file(filename)
  Oj.load(File.read(filename))
end

# Eventually if I were importing different data structures individually instead
# of doing just a batch import I'd separate this out into different files and
# commands, in this case we can just breeze through the decoded JSON and import
# it straight into the database.
def import_accommodations(filename = 'accommodations.json')
  json_from_file(filename)
    .map do |accommodation|
      [
        accommodation.fetch('id'),
        accommodation.dig('names', 'fallback') # #fetch will raise an error if the key is missing while #dig just returns nil
      ]
    end
    .then do |accommodations|
      # This is automatically wrapped in a transaction
      DB[:accommodations].import([:id, :name], accommodations)
    end
end

def import_accommodation_reviews(filename = 'reviews.json')
  json_from_file(filename)
    .map do |review|
      [
        review.fetch('id'),
        review.fetch('parents').first.fetch('id'),
        review.dig('user', 'id'),
        Time.at(0, review.fetch('travelDate'), :millisecond),
        review.fetch('ratings').fetch('general').fetch('general'),
        Oj.dump(review.fetch('ratings').fetch('aspects'))
      ]
    end
    .then do |reviews|
      DB[:accommodation_reviews].import(
        [:id, :accommodation_id, :user_id, :travel_date, :general_rating, :aspect_ratings],
        reviews
      )
    end
end

def clean_database
  DB[:accommodation_reviews].truncate
  DB[:accommodations].truncate
end

logger.info('setting up the database...')
create_tables

if ARGV[0] == '--clean'
  logger.info('clearing the database...')
  clean_database
end

logger.info('importing accommodations...')
import_accommodations
logger.info('importing accommodation reviews...')
import_accommodation_reviews
logger.info('done!')
